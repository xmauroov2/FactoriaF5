const express = require('express');
const exphbs = require('express-handlebars');
const app = express();

app.engine('.hbs', exphbs({
  defaultLayout: 'template',
  extname: '.hbs',
  partialsDir: ['views/partials']
}));

app.set('view engine', '.hbs');
app.get('/', (req, res) => {
  res.render('index');
});

app.use(express.static ('/stylesheets'));

app.listen(3000, () => {
  console.log('El puerto esta ready en 3000')
});
